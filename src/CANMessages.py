#!/usr/bin/python
# CANMessages.py

from scapy.layers.can import SignalPacket, LESignedSignalField, \
    LEUnsignedSignalField, SignalHeader, BEUnsignedSignalField
from scapy.packet import bind_layers


class MPH_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("mph", default=0, start=0, size=12, scaling=1, unit="mph"),
    ]


bind_layers(SignalHeader, MPH_pkt, identifier=0x143)


class RPM_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("rpm", default=0, start=8, size=32, scaling=1, unit="rpm"),
    ]


bind_layers(SignalHeader, RPM_pkt, identifier=0x3ff)


class TEMP_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("temp", default=0, start=16, size=12, scaling=1, offset=20, unit="deg C"),
    ]


bind_layers(SignalHeader, TEMP_pkt, identifier=0x156)


class OIL_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("level", default=0, start=4, size=32, scaling=1, offset=10, unit="l"),
    ]


bind_layers(SignalHeader, OIL_pkt, identifier=0x116)


class AAC_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("aac", default=0, start=24, size=16, scaling=1),
    ]


bind_layers(SignalHeader, AAC_pkt, identifier=0x342)


class FUEL_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("fuel", default=0, start=0, size=16, scaling=1, unit="l"),
    ]


bind_layers(SignalHeader, FUEL_pkt, identifier=0x210)


class IND_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("ind", default=0, start=33, size=1, scaling=1),
    ]


bind_layers(SignalHeader, IND_pkt, identifier=0x4ac)


class IND2_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("ind", default=0, start=34, size=1, scaling=1),
    ]


bind_layers(SignalHeader, IND2_pkt, identifier=0x4bc)


class KM_pkt(SignalPacket):
    fields_desc = [
        LESignedSignalField("km", default=0, start=4, size=32, scaling=1, unit="km"),
    ]


bind_layers(SignalHeader, KM_pkt, identifier=0x234)


class KM_VAL_pkt(SignalPacket):
    fields_desc = [
        BEUnsignedSignalField("km", default=0, start=4, size=32, scaling=1, unit="km"),
    ]


bind_layers(SignalHeader, KM_VAL_pkt, identifier=0x432)
