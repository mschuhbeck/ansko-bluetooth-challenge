#!/usr/bin/env python3

import bluetooth
import can
import threading


class DiagnosticsServer(threading.Thread):
    def send_command_to_can_bus(self, bus, data):
        # Parse the data to extract the CAN message parameters
        id, data = data.split()
        id = int(id, 16)
        data = [int(byte, 16) for byte in data.split(":")]

        # Create a CAN message and send it on the bus
        msg = can.Message(arbitration_id=id, data=data)
        print(msg)
        bus.send(msg)

    def run(self):
        while True:
            try:
                server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
                server_sock.bind(("", bluetooth.PORT_ANY))
                server_sock.listen(1)

                port = server_sock.getsockname()[1]

                uuid = "ffe54d68-cdb4-4eb6-8751-f7b321f516d8"

                bluetooth.advertise_service(server_sock, "DiagnosticsServer", service_id=uuid,
                                            service_classes=[uuid, bluetooth.SERIAL_PORT_CLASS],
                                            profiles=[bluetooth.SERIAL_PORT_PROFILE],
                                            )

                print("Waiting for connection on RFCOMM channel", port)

                client_sock, client_info = server_sock.accept()
                print("Accepted connection from", client_info)

                canbus = can.Bus(bustype='socketcan', channel='can1', bitrate=500000)

                while True:
                    data = client_sock.recv(1024).strip().decode("utf-8")

                    if data:
                        print("Received command: ", data)
                        self.send_command_to_can_bus(canbus, data)
            except bluetooth.btcommon.BluetoothError:
                client_sock.close()
                server_sock.close()
                print("Client Disconnected.")


if __name__ == '__main__':
    ds = DiagnosticsServer()
    ds.start()