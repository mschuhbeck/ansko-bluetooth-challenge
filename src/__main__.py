#!/usr/bin/python

import sys
import os.path

try:
    if os.path.exists('/home/hackerman/.plattform/master'):
        import Dashboard
    else:
        raise ImportError
except ImportError:
    print('Dashboard could not be imported.')

try:
    if os.path.exists('/home/hackerman/.plattform/slave'):
        import DiagnosticsServer
    else:
        raise ImportError
except ImportError:
    print('DS could not be imported.')

def main():
    print("Starting")
    if os.path.exists('/home/hackerman/.plattform/master'):
        Dashboard.start()
    elif os.path.exists('/home/hackerman/.plattform/slave'):
        ds = DiagnosticsServer.DiagnosticsServer()
        ds.start()
    else:
        print("Error: master and slave file not found")
        sys.exit(2)


if __name__ == "__main__":
    main()
