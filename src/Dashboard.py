#!/usr/bin/python
# dashboard.py

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

import os
import pygame
from pygame.locals import *
import pygame.gfxdraw
from colored import *
import string

os.environ['SDL_VIDEO_WINDOW_POS'] = 'center'

from scapy.all import *

# Scapy configuration
conf.contribs['CANSocket'] = {'use-python-can': False}

from scapy.contrib.cansocket import CANSocket
from scapy.layers.can import SignalHeader
from CANMessages import *

lock = threading.Lock()

THREAD_SHOULD_RUN = True

# globals controlling the dials
MPH_Value = 0
RPM_Value = 0
TEMP_Value = 0
OILLEVEL_Value = 0
AAC_Value = 0  # Automatic Air Conditioning
FUEL_Value = 0  # Mass Air Flow
IND_Value = 0  # Indicator light
IND2_Value = 0 # Indicator light 2
KM_Value = 10000
KM_VAL_Value = 10000
Text_Value = ''


BLACK = (0, 0, 0)
RED = (30, 0, 0)
PINK = (255, 105, 180)
PURPLE = (128, 0, 128)
WHITE = (255, 255, 255)
BLUE = (136, 196, 255)
GRAY = (128, 128, 128)
GREEN = (0, 255, 0)

percent = u'\N{PERCENT SIGN}'
fuellevel = 'l'
volt = 'V'

degree = u"\u00B0"

fifteen = None
twenty = None
sixty = None


class DashboardCANReq(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._socket = CANSocket(channel="can1", basecls=SignalHeader)
        self._running = True

    def run(self):
        while self._running:
            self._socket.send(SignalHeader()/IND_pkt(ind=1))
            time.sleep(0.1)

            self._socket.send(SignalHeader()/IND2_pkt(ind=1))
            time.sleep(0.1)

    def stop(self):
        self._running = False
        self._socket.close()


def receivefkt(pkt):
    if pkt.flags == 'remote_transmission_request':
        return

    data = pkt[1]

    if isinstance(data, MPH_pkt):
        lock.acquire()
        global MPH_Value
        MPH_Value = data.mph
        lock.release()
    elif isinstance(data, RPM_pkt):
        lock.acquire()
        global RPM_Value
        RPM_Value = data.rpm
        lock.release()
    elif isinstance(data, TEMP_pkt):
        lock.acquire()
        global TEMP_Value
        TEMP_Value = data.temp
        lock.release()
    elif isinstance(data, OIL_pkt):
        lock.acquire()
        global OILLEVEL_Value
        OILLEVEL_Value = data.level
        lock.release()
    elif isinstance(data, FUEL_pkt):
        lock.acquire()
        global FUEL_Value
        FUEL_Value = data.fuel
        lock.release()
    elif isinstance(data, KM_pkt):
        lock.acquire()
        global KM_Value
        KM_Value = data.km
        lock.release()
    elif isinstance(data, AAC_pkt):
        lock.acquire()
        global AAC_Value
        AAC_Value = data.aac
        lock.release()
    elif isinstance(data, KM_VAL_pkt):
        lock.acquire()
        global KM_VAL_Value
        KM_VAL_Value = data.km
        lock.release()


class DashboardCANRecv(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self._socket = CANSocket(channel="can1", basecls=SignalHeader)
        self._running = True

    def run(self):
        while self._running:
            self._socket.sniff(timeout=0.3, prn=receivefkt)

    def stop(self):
        self._running = False
        self._socket.close()


def indicatorLegend(
        legendValue,
        displayValue,
        positionX,
        positionY,
        length,
        destination,
        fontSize,
        doubleLength=False,
        drawLine=True,
        doubleLine=6,
        singleLine=3,
        displayDivision=1,
        backgroundColour=RED,
        dialType=False
):
    position = (positionX, positionY)

    if doubleLength:
        lineLength = doubleLine
    else:
        lineLength = singleLine

    x = position[0] - math.cos(math.radians(legendValue)) * length
    y = position[1] - math.sin(math.radians(legendValue)) * length
    xa = position[0] - math.cos(math.radians(legendValue)) * (length - int(length / lineLength))
    ya = position[1] - math.sin(math.radians(legendValue)) * (length - int(length / lineLength))
    xlabel = position[0] - math.cos(math.radians(legendValue)) * (length - int(length / singleLine))
    ylabel = position[1] - math.sin(math.radians(legendValue)) * (length - int(length / singleLine))

    if drawLine:
        pygame.draw.aaline(destination, BLUE, (x, y), (xa, ya), False)

    if dialType:
        label = fontSize.render(("%s%s" % ((displayValue / displayDivision), dialType)),
                                1, BLUE, backgroundColour)
    else:
        label = fontSize.render((str(displayValue / displayDivision)),
                                1, BLUE, backgroundColour)

    labelRect = label.get_rect()
    labelRect.centerx = int(xlabel)
    labelRect.centery = int(ylabel)  # + 5
    destination.blit(label, (labelRect))


def indicatorNeedle(
        needleDestination,
        needleValue=0,
        needleLength=358,
        positionX=600,
        positionY=360,
        fontSize=sixty,
        backgroundColour=BLACK,
        startPosition=0,
        endPosition=0,
        maximumValue=10,
        doubleLine=6,
        singleLine=3,
        displayDivision=1,
        displayNeedle=True,
        displayCircle=True,
        dialLabel=False,
        dialType=False
):
    position = (positionX, positionY)
    length = needleLength
    length2 = int(needleLength / 20)
    length3 = length2 + 5
    destination = needleDestination
    fontSize = fontSize
    singleLine = singleLine
    doubleLine = doubleLine
    backgroundColour = backgroundColour

    degreesDifference = 360 - (startPosition + (180 - endPosition))
    value = int((needleValue * (degreesDifference / (maximumValue * 10.0))) + startPosition)
    displayValue = (needleValue * (degreesDifference / (maximumValue * 10.0))) + startPosition

    x = position[0] - math.cos(math.radians(value)) * (length - int(length / singleLine))
    y = position[1] - math.sin(math.radians(value)) * (length - int(length / singleLine))
    x2 = position[0] - math.cos(math.radians(value - 90)) * length2
    y2 = position[1] - math.sin(math.radians(value - 90)) * length2
    x3 = position[0] - math.cos(math.radians(value + 180)) * length3
    y3 = position[1] - math.sin(math.radians(value + 180)) * length3
    x4 = position[0] - math.cos(math.radians(value + 90)) * length2
    y4 = position[1] - math.sin(math.radians(value + 90)) * length2

    xa = position[0] - math.cos(math.radians(value)) * length
    ya = position[1] - math.sin(math.radians(value)) * length
    xa2 = x - math.cos(math.radians(value))
    ya2 = y - math.sin(math.radians(value))
    xa3 = x - math.cos(math.radians(value + 180))
    ya3 = y - math.sin(math.radians(value + 180))
    xa4 = x - math.cos(math.radians(value + 90)) * (length2 + 4)
    ya4 = y - math.sin(math.radians(value + 90)) * (length2 + 4)

    if displayCircle:
        pygame.gfxdraw.aacircle(destination,position[0],position[1], length, RED)
        pygame.draw.circle(destination, RED, (position[0], position[1]), length, 0)

    if dialLabel:
        showLabel = fontSize.render(dialLabel, 1, BLUE, backgroundColour)
        labelRect = showLabel.get_rect()
        labelRect.centerx = position[0]
        labelRect.centery = position[1] - 30
        destination.blit(showLabel, (labelRect))

    valueDivisions = degreesDifference / 10

    indicatorLegend(startPosition, 0, position[0], position[1], length,
                    destination, fontSize, False, True, doubleLine, singleLine, 1, backgroundColour)

    for divisions in range(1, 10):

        if needleValue >= (maximumValue * divisions):
            indicatorLegend((startPosition + (valueDivisions * divisions)), (maximumValue * divisions),
                            position[0], position[1], length, destination, fontSize, True, True, doubleLine
                            , singleLine, displayDivision, backgroundColour)

    if displayNeedle:
        pygame.draw.aalines(destination, BLUE, True, ((x, y), (x2, y2), (x3, y3), (x4, y4)), False)

    pygame.gfxdraw.arc(destination, position[0], position[1],
                       (length - int(length / singleLine)), (180 + value), endPosition, BLUE)

    pygame.draw.aaline(destination, BLUE, (x, y), (xa, ya), False)

    pygame.gfxdraw.arc(destination, position[0], position[1],
                       length, (180 + startPosition), (value - 180), BLUE)

    pygame.gfxdraw.arc(destination, position[0], position[1],
                       (length - int(length / doubleLine)), (180 + startPosition), (value - 180), BLUE)

    if dialType:
        indicatorLegend((180 + endPosition), needleValue, position[0], position[1],
                        length, destination, fontSize, False, False,
                        doubleLine, singleLine, 1, backgroundColour, dialType)
    else:
        indicatorLegend((180 + endPosition), needleValue, position[0], position[1],
                        length, destination, fontSize, False, False,
                        doubleLine, singleLine, 1, backgroundColour)

def display_text(screen, text, font, x, y, color):
    """
    Simply prints text on the designated pygame screen at the designated
    x and y positions.
    :param screen: pygame screen to be printed on
    :param text: text to be printed
    :param font: font used for printing
    :param x: x coordinate for text
    :param y: y coordinate for text
    :param color: color of the text to be printed
    :return:
    """
    textsurface = font.render(text, False, color)
    screen.blit(textsurface, (x, y))


def randomStringDigits(stringLength=6):
    """
    Generate a random string of letters and digits
    """
    lettersAndDigits = string.ascii_letters + string.digits
    return ''.join(random.choice(lettersAndDigits) for i in range(stringLength))


def start():
    """
    Starts the graphical user interface for the challenge.
    Displays the flag if challenge solved successfully.
    """
    pygame.init()

    # Start the CAN request Thread
    reqThread = DashboardCANReq()
    reqThread.start()

    recvThread = DashboardCANRecv()
    recvThread.start()
    label_font = pygame.font.SysFont("Droid Sans", 32)


    size = window_width, window_height = 1320, 740
    monitor_width = pygame.display.Info().current_w
    monitor_height = pygame.display.Info().current_h

    surface1WindowedX = (window_width / 2) - 650
    surface1WindowedY = (window_height / 2) - 360

    surface1X = surface1WindowedX
    surface1Y = surface1WindowedY

    surface2WindowedX = (window_width / 2) - 500
    surface2WindowedY = (window_height / 2) - 210

    surface2X = surface2WindowedX
    surface2Y = surface2WindowedY

    surface3WindowedX = (window_width / 2) - 650
    surface3WindowedY = (window_height / 2) - 360

    surface3X = surface3WindowedX
    surface3Y = surface3WindowedY

    surface4WindowedX = (window_width / 2) + 310
    surface4WindowedY = (window_height / 2) - 360

    surface4X = surface4WindowedX
    surface4Y = surface4WindowedY

    surface5WindowedX = (window_width / 2) - 310
    surface5WindowedY = (window_height / 2) + 60

    surface5X = surface5WindowedX
    surface5Y = surface5WindowedY

    surface6WindowedX = (window_width / 2) + 10
    surface6WindowedY = (window_height / 2) + 60

    surface6X = surface6WindowedX
    surface6Y = surface6WindowedY

    surface_imgX_Windowed = (window_width / 2)
    surface_imgY_Windowed = (window_height / 2)

    surface_imgX = surface_imgX_Windowed
    surface_imgY = surface_imgY_Windowed

    surface_img2X_Windowed = (window_width / 2) - 80
    surface_img2Y_Windowed = (window_height / 2)

    surface_img2X = surface_img2X_Windowed
    surface_img2Y = surface_img2Y_Windowed

    labelWindowedX = 30
    labelWindowedY = window_height - 40

    labelX = labelWindowedX
    labelY = labelWindowedY

    screen = pygame.Surface((window_width, window_height))
    destination_size = (800, 480)
    real_screen = pygame.display.set_mode(destination_size)
    pygame.display.set_caption("Dashboard")

    surface1 = pygame.Surface((1300, 720))
    surface2 = pygame.Surface((1000, 600))
    surface3 = pygame.Surface((340, 340))
    surface4 = pygame.Surface((340, 340))
    surface5 = pygame.Surface((300, 300))
    surface6 = pygame.Surface((300, 300))

    surface2.set_colorkey(0x0000FF)
    surface3.set_colorkey(0x0000FF)
    surface4.set_colorkey(0x0000FF)
    surface5.set_colorkey(0x0000FF)
    surface6.set_colorkey(0x0000FF)

    screen.fill(0x000000)

    fifteen = pygame.font.SysFont("Droid Sans", 15)

    twenty = pygame.font.SysFont("Droid Sans", 18)

    sixty = pygame.font.SysFont("Droid Sans", 60)

    # load check engine indicator image
    #indlight_img = pygame.image.load("check-engine-light-1.jpg")
    #indlight_img = pygame.transform.smoothscale(indlight_img, (50, 50))
    # load door indicator image
    doorlight_img = pygame.image.load("doors-open-light.jpg")
    doorlight_img = pygame.transform.smoothscale(doorlight_img, (65, 65))

    km_display_top = screen.get_height() / 2 + 30
    km_display_left = screen.get_width() / 2 - 110
    km_display_width = 200
    km_display_height = 40

    pygame.display.set_mode(destination_size)
    screen.fill(0x000000)
    pygame.mouse.set_visible(True)

    while True:
        pygame.time.Clock().tick(60)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                THREAD_SHOULD_RUN = False
                sys.exit()

            if event.type is KEYDOWN and event.key == K_q:
                THREAD_SHOULD_RUN = False
                sys.exit()

            if event.type is KEYDOWN and event.key == K_w:
                pygame.display.set_mode(destination_size)
                pygame.mouse.set_visible(False)
                surface1X = surface1WindowedX
                surface1Y = surface1WindowedY
                surface2X = surface2WindowedX
                surface2Y = surface2WindowedY
                surface3X = surface3WindowedX
                surface3Y = surface3WindowedY
                surface4X = surface4WindowedX
                surface4Y = surface4WindowedY
                surface5X = surface5WindowedX
                surface5Y = surface5WindowedY
                surface6X = surface6WindowedX
                surface6Y = surface6WindowedY
                surface_imgX = surface_imgX_Windowed
                surface_imgY = surface_imgY_Windowed
                surface_img2X = surface_img2X_Windowed
                surface_img2Y = surface_img2Y_Windowed
                labelX = labelWindowedX
                labelY = labelWindowedY
                screen.fill(0x000000)

            if event.type is KEYDOWN and event.key == K_f:
                pygame.display.set_mode(destination_size, FULLSCREEN)
                screen.fill(0x000000)
                pygame.mouse.set_visible(False)


        surface1.fill(0x000000)
        surface2.fill(0x0000FF)
        surface3.fill(0x0000FF)
        surface4.fill(0x0000FF)
        surface5.fill(0x0000FF)
        surface6.fill(0x0000FF)

        indicatorNeedle(surface1, MPH_Value, 648, 650, 650, sixty, BLACK, 0, 0, 10, 12, 6, 1, False, False)
        indicatorNeedle(surface2, RPM_Value, 488, 500, 500, sixty, BLACK, 0, 0, 500, 10, 5, 100, False, False)
        indicatorNeedle(surface3, FUEL_Value, 168, 170, 170, twenty, BLACK, -45, -45, 50, 6, 3, 10, True, False, "Fuel", fuellevel)
        #indicatorNeedle(surface4, AAC_Value, 168, 170, 170, twenty, BLACK, 45, 45, 10, 6, 3, 1, True, False, "AAC", percent)
        indicatorNeedle(surface5, TEMP_Value, 148, 150, 150, twenty, BLACK, -45, 45, 16, 6, 3, 1, True, False, "Oil Temp", degree)
        indicatorNeedle(surface6, OILLEVEL_Value, 148, 150, 150, twenty, BLACK, -45, 45, 2, 6, 3, 1, True, False, "Oil Level", "l")

        screen.blit(surface1, (surface1X, surface1Y))
        screen.blit(surface2, (surface2X, surface2Y))
        screen.blit(surface3, (surface3X, surface3Y))
        screen.blit(surface4, (surface4X, surface4Y))
        screen.blit(surface5, (surface5X, surface5Y))
        screen.blit(surface6, (surface6X, surface6Y))
        # ind light blits
        if IND_Value == 1:
            screen.blit(indlight_img, (surface_imgX, surface_imgY))
        if IND2_Value == 1:
            screen.blit(doorlight_img, (surface_img2X, surface_img2Y))

        doors_text = pygame.font.SysFont("arial", 20)

        km_display = pygame.draw.rect(screen, GRAY, (km_display_left, km_display_top, km_display_width, km_display_height))
        display_text(screen, str(KM_Value) + "km", doors_text, km_display_left + 60, km_display_top + 10, BLACK)

        if(KM_Value == (KM_VAL_Value - 50000)):
            global Text_Value
            Text_Value = "Solved"

        # text label
        label = label_font.render(Text_Value, 1, BLUE)
        display_text(screen, Text_Value, label_font, 10, screen.get_height() - 40, GREEN)

        pygame.transform.smoothscale(screen, destination_size, real_screen)

        pygame.display.update()
