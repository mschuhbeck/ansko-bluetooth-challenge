# ansko-bluetooth

## ANSKO
### dependencies:

master pi:
- colored
- scapy

slave pi:
- pybluez
- python-can

### service needs to be started manually on slave pi:
```
systemctl start bluetooth-challenge.service
```

### may be helpful:
- https://stackoverflow.com/questions/37913796/bluetooth-error-no-advertisable-device

## Laptop

### client usage:
```
python3 rfcomm-client.py UUID MAC
```

### Preparations/Troubleshooting:
- bluetooth.btcommon.BluetoothError: (2, 'No such file or directory')

1. Run bluetooth in compatibility mode, by modifying
```
/etc/systemd/system/dbus-org.bluez.service,
```
changing
```
ExecStart=/usr/lib/bluetooth/bluetoothd
```
into
```
ExecStart=/usr/lib/bluetooth/bluetoothd -C
```
2. Then add the Serial Port Profile, executing: 
```
sudo sdptool add SP
```
3. Reboot system

(https://stackoverflow.com/questions/36675931/bluetooth-btcommon-bluetootherror-2-no-such-file-or-directory)

- Python3 error "no module named bluetooth" https://stackoverflow.com/questions/23985163/python3-error-no-module-named-bluetooth-on-linux-mint
```
sudo apt-get install bluetooth libbluetooth-dev
sudo python3 -m pip install pybluez
```
