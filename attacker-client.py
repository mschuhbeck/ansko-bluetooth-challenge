#!/usr/bin/env python3

import sys
import bluetooth


addr = sys.argv[2]
print("Searching for DiagnosticsServer on {}...".format(addr))

# search for the DiagnosticsServer
uuid = sys.argv[1]
service_matches = bluetooth.find_service(uuid=uuid, address=addr)

if len(service_matches) == 0:
    print("Couldn't find the DiagnosticsServer.")
    sys.exit(0)

first_match = service_matches[0]
port = first_match["port"]
name = first_match["name"]
host = first_match["host"]

print("Connecting to \"{}\" on {}".format(name, host))

# Create the client socket
sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
sock.connect((host, port))

print("Connected. Type a CAN message, in format: ID XX:XX:XX...")

while True:
    data = input()
    if not data:
        break
    sock.send(data)

sock.close()